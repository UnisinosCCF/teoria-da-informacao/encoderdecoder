Trabalho de Teoria da Informação - Universidade do Vale do Rio dos Sinos

Alunos: Cristiano Farias e Fabiane Kuhn

Semestre: 2020/2

O projeto visa desenvolver encoders e decoders a partir das estratégias: Golomb, Elias-Gamma, Fibonacci, Unária e Delta. Além disso, foram implementados códigos de tratamento de ruídos.

Para a aplicação foi utilizada a linguagem Java, em sua versão 14.

Branch para execução: master

AO PROFESSOR:

-Não conseguimos finalizar o encode e decode Delta

-Foi disponibilizado via entrega da tarefa no moodle o JAR executavel java da aplicação.

Para executar a aplicação é necessario se ter no SO o Java versão 8 ou superior

Linhas de comando para execução

Para um Encode:

Parametros:
<br>
<br>1 = E (encode)<br>
    2 = Tipo de codificacao (0: Golomb, 1:Elias-Gamma, 2:Fibonacci e 3:Unária)<br>
    3 = Digito divisor (Golomb)<br>
    4 = caminho do arquivo de entrada (path)<br>
    5 = usar Hamming pós encode (0 ou 1)<br><br>
Exemplo codificando em Golomb com divisor 4 usando hamming ao final:<br>
    > java -jar "arquivo_jar_projeto.jar" E 0 4 "documentos\alice29.txt" 1<br>

Para um Decode:<br>
    Parametros:<br>
    1 = D (Decode)<br>
    2 = caminho do arquivo de entrada (path)<br>
    3 = usar Hamming pós encode (0 ou 1)<br><br>
Exemplo decodificando o arquivo gerado no exemplo acima usando hamming:<br>
    > java -jar "arquivo_jar_projeto.jar" D "documentos\alice29.txt.ecc" 1<br>