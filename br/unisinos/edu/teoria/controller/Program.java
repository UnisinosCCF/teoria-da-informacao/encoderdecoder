package br.unisinos.edu.teoria.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import br.unisinos.edu.teoria.model.algorithm.Delta;
import br.unisinos.edu.teoria.model.algorithm.EliasGama;
import br.unisinos.edu.teoria.model.algorithm.Fibonacci;
import br.unisinos.edu.teoria.model.algorithm.Golomb;
import br.unisinos.edu.teoria.model.algorithm.Unary;
import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.model.roots.IDecoder;
import br.unisinos.edu.teoria.model.roots.IEncoder;
import br.unisinos.edu.teoria.model.roots.IProgram;
import br.unisinos.edu.teoria.util.CRC8;
import br.unisinos.edu.teoria.util.Util;

public class Program implements IProgram {

	private boolean printReadContent = false;

	@Override
	public void write(boolean append, Path pathFile, byte... bytes) throws IOException {
		File f = pathFile.toFile();
		if (!f.exists()) {
			f.getParentFile().mkdirs();
			f.createNewFile();
		}
		if (append) {
			Files.write(pathFile, bytes, StandardOpenOption.APPEND);
		} else {
			Files.write(pathFile, bytes);
		}
	}

	@Override
	public byte[] read(Path pathFile) throws IOException {
		// Lendo bytes do arquivo de entrada.
		byte[] bytes = null;
		// Util.pathResources("arquivo")
		bytes = Files.readAllBytes(pathFile);
		return bytes;
	}

	/**
	 * Metodo para linkar type com classe que implementa a solucao.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de set. de 2020
	 * @param type
	 * @return
	 */
	private EncoderDecoderProgram getImplByType(EncondingType type, Integer... attrs) {
		switch (type) {
		case Golomb:
			if (attrs == null || attrs.length < 1) {
				Util.exit("Parametro de divisor não informado.");
			}
			return new Golomb(printReadContent, attrs[0]);
		case Delta:
			return new Delta(printReadContent);
		case EliasGama:
			return new EliasGama(printReadContent);
		case Fibonacci:
			return new Fibonacci(printReadContent);
		case Unary:
			return new Unary(printReadContent);
		default:
			Util.exit("Type nao identificado corretamente.");
			return null;
		}
	}

	@Override
	/**
	 * Faz a leitura dos dois bytes do cabecalho META e retorna o array de bytes
	 * apenas do conteudo.
	 */
	public byte[] headerRead(byte[] bytes) throws IOException {
		if (bytes.length <= 2) {
			Util.exit("Não há bytes suficientes para realizar esta decodificacao...");
		}
		return Arrays.copyOfRange(bytes, 0, 2);
	}

	public byte[] checkHamming(byte[] bytes) {

		// Se inicia pela checagemde cabeçalho e CRC
		if (bytes.length < 12) {
			Util.exit("Não há bytes suficientes para realizar esta decodificacao...");
		}
		byte tipoEncode = bytes[0];
		byte divisor = bytes[1];
		int tamanhoSomadoNofinal = Integer.parseInt(((char) bytes[2]) + "" + ((char) bytes[3]), 2);
		byte[] saida = {};
		saida = Util.updateOutput(saida, tipoEncode, divisor);
//		 Cria o CRC-8
		String bin = CRC8.getCRC8InBinary(tipoEncode, divisor, bytes[2], bytes[3]);
		String binInFile = new String(Arrays.copyOfRange(bytes, 4, 12));
		System.out.println("Comparando checksum: " + bin.equals(binInFile));
		if (!bin.equals(binInFile)) {
			System.out.println("Problemas no cabeçalho META, checksum com falha.");
			Util.exit("Problemas no cabeçalho META, checksum com falha.");
		}
		byte[] conteudo = Arrays.copyOfRange(bytes, 12, bytes.length);
		int resto = conteudo.length % 7;
		if (resto != 0) {
			Util.exit("Problemas no conteudo para decodificar.");
		}

		for (int i = 0; i < conteudo.length; i++) {
			byte b1 = conteudo[i];
			byte b2 = conteudo[i + 1];
			byte b3 = conteudo[i + 2];
			byte b4 = conteudo[i + 3];
			byte b_paridade1 = conteudo[i + 4];
			byte b_paridade2 = conteudo[i + 5];
			byte b_paridade3 = conteudo[i + 6];

			byte paridade1 = byteParidade(b1, b2, b3);
			byte paridade2 = byteParidade(b2, b3, b4);
			byte paridade3 = byteParidade(b1, b3, b4);

			boolean val_p1 = (b_paridade1 == paridade1);
			boolean val_p2 = (b_paridade2 == paridade2);
			boolean val_p3 = (b_paridade3 == paridade3);

			// Se tiver um erro tem que ajustar
			if (!(val_p1 && val_p2 && val_p3)) {
				if (val_p1 == false && val_p2 == false && val_p3 == false) {
					// 3 ta errado;
					System.out.println("Ruido detectado no Bit 3, ajustando...");
					b3 = invertBinary(b3);
				} else if (val_p1 == false) {
					// Aqui o erro ta no proprio de paridade
					if (val_p2 && val_p3) {
						System.out.println("Falha em Bit de paridade 1, ajustando...");
						paridade1 = invertBinary(paridade1);
						// è o 2
					} else if (val_p2 == false) {
						System.out.println("Ruido detectado no Bit 2, ajustando...");
						b2 = invertBinary(b2);
					} else {
						System.out.println("Ruido detectado no Bit 1, ajustando...");
						b1 = invertBinary(b1);
					}
				} else if (val_p2 == false) {
					// Aqui o erro ta no proprio de paridade
					if (val_p1 && val_p3) {
						System.out.println("Falha em Bit de paridade 2, ajustando...");
						paridade2 = invertBinary(paridade2);
						// è o 4
					} else {
						System.out.println("Ruido detectado no Bit 4, ajustando...");
						b4 = invertBinary(b4);
					}
				} else if (val_p3 == false) {
					// Aqui o erro ta no proprio de paridade
					if (val_p1 && val_p2) {
						System.out.println("Falha em Bit de paridade 3, ajustando...");
						paridade3 = invertBinary(paridade3);
					}
				}
			}

			saida = Util.updateOutput(saida, b1, b2, b3, b4);

			i = i + 6;
		}

		// Remover o final
		saida = Arrays.copyOfRange(saida, 0, (saida.length - tamanhoSomadoNofinal));

//		saida = Util.updateOutput(saida, new byte[] { bytes[0], bytes[1] });
//		saida = Util.updateOutput(saida, new byte[] { sizeComplete[0], sizeComplete[1] });
//		saida = Util.updateOutput(saida, bin.getBytes());
		return saida;
	}

	private byte invertBinary(byte bytee) {
		char bit = (char) bytee;
		if (bit == '0') {
			return '1';
		} else if (bit == '1') {
			return '0';
		}
		Util.exit("Atenção para esta troca apenas é permitido 1 ou 0");
		return 'X';
	}

	public byte[] applyHamming(byte[] bytes) {

		byte[] saida = {};
		byte[] content = Arrays.copyOfRange(bytes, 2, bytes.length);
		byte[] sizeComplete = { '0', '0' };

		// checksize
		int resto = (bytes.length - 2) % 4;
		if (resto > 0) {
			int diff = (4 - resto);
			content = Util.updateOutput(content, Util.addToLeft("", "0", diff).getBytes());
			// Adiciona no cabeçalho a informação de completar o hamming
			String bin = Integer.toBinaryString(diff);
			if (bin.length() < 2) {
				bin = Util.addToLeft(bin, "0", 2 - bin.length());
			}
			sizeComplete = bin.getBytes();
		}

		// Cria o CRC-8
		String bin = CRC8.getCRC8InBinary(bytes[0], bytes[1], sizeComplete[0], sizeComplete[1]);
		saida = Util.updateOutput(saida, new byte[] { bytes[0], bytes[1] });
		saida = Util.updateOutput(saida, new byte[] { sizeComplete[0], sizeComplete[1] });
		saida = Util.updateOutput(saida, bin.getBytes());

		// Iniciando logica de paridades
		for (int i = 0; i < content.length; i++) {

			byte b1 = content[i];
			byte b2 = content[i + 1];
			byte b3 = content[i + 2];
			byte b4 = content[i + 3];

			byte paridade1 = byteParidade(b1, b2, b3);
			byte paridade2 = byteParidade(b2, b3, b4);
			byte paridade3 = byteParidade(b1, b3, b4);

			saida = Util.updateOutput(saida, b1, b2, b3, b4, paridade1, paridade2, paridade3);

			i = i + 3;
		}

		return saida;
	}

	private byte byteParidade(byte b1, byte b2, byte b3) {
		int v1 = Character.getNumericValue((char) b1);
		int v2 = Character.getNumericValue((char) b2);
		int v3 = Character.getNumericValue((char) b3);
		int divisao = (v1 + v2 + v3) % 2;
		if (divisao == 0) {
			return '0';
		} else {
			return '1';
		}
	}

	/**
	 * Metodo responsavel por inicial a rotina de encode de uma arquivo.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de set. de 2020
	 * @param fileName
	 * @throws IOException
	 */
	public void startEncode(Path pathFileIN, Path pathFileOUT, EncondingType type, boolean eccHamming, Integer... attrs) throws IOException {

		System.out.println("====================================================================");
		System.out.println("Iniciando codificacao...");
		System.out.println("Encode de tipo " + type.name() + "...");

		System.out.println("Paths:");
		System.out.println("\t-> Path  in: " + pathFileIN);
		System.out.println("\t-> Path out: " + pathFileOUT);

		// read in
		System.out.println("Lendo bytes de entrada...");
		byte[] bytes = read(pathFileIN);

		// Encode (iniciando rotina de codificacao do conteudo)
		IEncoder encoder = getImplByType(type, attrs);

		System.out.println("Iniciado arquivo de saida...");

		// Write Header
		System.out.println("Escrevendo cabecalho...");
		byte[] finalByte = encoder.headerWrite();

		// Chama o metodo de encode da interface. que executa a logica da classe de
		// codificacao correta.
		finalByte = Util.updateOutput(finalByte, encoder.encode(bytes));

		if (eccHamming) {
			System.out.println("Opção de tratamento de ruidos Hamming sendo aplicada...");
			finalByte = applyHamming(finalByte);
			pathFileOUT = Util.changeSufixFile(pathFileOUT, ".ecc");
		}

		// Write content
		System.out.println("Escrevendo arquivo de saida...");
		write(pathFileOUT, finalByte);

		// Finalizado
		System.out.println("Processo de Encode finalizado...");
		System.out.println("Arquivo de saida: \n\t-> " + pathFileOUT);
		System.out.println("====================================================================");
	}

	public void startDecode(Path pathFileIN, Path pathFileOUT, boolean eccHamming) throws IOException {
		System.out.println("====================================================================");
		System.out.println("Iniciando decodificacao...");

		System.out.println("Paths:");
		System.out.println("\t-> Path  in: " + pathFileIN);
		System.out.println("\t-> Path out: " + pathFileOUT);

		// read in
		System.out.println("Lendo bytes de entrada...");
		byte[] bytes = read(pathFileIN);

		if (eccHamming) {
			System.out.println("Opção de tratamento de ruidos Hamming sendo aplicada...");
			bytes = checkHamming(bytes);
		}

		System.out.println("Iniciando leitura de cabecalho META...");
		byte[] header = headerRead(bytes);

		EncondingType type = EncondingType.getByCode(header[0]);
		System.out.println("Tipo de codificacao identificada: " + type);

		int divisor = 0;
		if (type == EncondingType.Golomb) {
			divisor = Integer.parseInt(new String(header, 1, 1));
			System.out.println("Divisor: " + divisor);
		}

		// Encode (iniciando rotina de codificacao do conteudo)
		IDecoder decode = getImplByType(type, divisor);

		// Chama o metodo de decode da interface. que executa a logica da classe de
		// codificacao correta.
		bytes = decode.decode(Arrays.copyOfRange(bytes, 2, bytes.length));

		// Write content
		System.out.println("Escrevendo arquivo de saida...");
		write(false, pathFileOUT, bytes);

		// Finalizado
		System.out.println("Processo de Decode finalizado...");
		System.out.println("Arquivo de saida: \n\t-> " + pathFileOUT);
		System.out.println("====================================================================");
	}

	public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException {

		if (args == null || args.length == 0) {

			// COLOCANDO ARGS NA MAO

			// Para ENCODE
			args = new String[5];
			args[0] = "E"; //
			args[1] = "0"; // Tipo de encode
			args[2] = "4"; // Divisor Golomb
			args[3] = Util.pathResources("arquivo").toFile().getAbsolutePath(); // Path
			args[4] = "1"; // Hamming on/off

			// Para DECODE
			// args[0] = "D"; //
			// args[1] =
			// Util.pathResources("Golomb_arquivo.ecc").toFile().getAbsolutePath(); // Path
			// args[2] = "1"; // Hamming on/off
		}

		// Print argumentos.
		for (final String arg : args) {

			System.out.println(arg);
		}

		if (args.length < 3) {
			Util.exit("Numero de argumentos invalidos");
		}

		if (args[0].length() != 1) {
			Util.exit("informe no primeiro argumento: E: encode ou D: decode...");
		}

		Program program = new Program();

		if (args[0].equalsIgnoreCase("E")) {

			if (args.length != 5) {
				Util.exit("Procedimento de Encode exige 5 parametros...");
			}
			try {
				Integer tipo = Integer.parseInt(args[1]);

				if (tipo >= 0 && tipo <= 3) {

					Integer divisor = Integer.parseInt(args[2]);

					Path path = Path.of(args[3]);

					try {
						Integer hamming = Integer.parseInt(args[4]);
						if (hamming.equals(1)) {
							program.startEncode(path, EncondingType.getByCode(tipo), true, divisor);
						} else if (hamming.equals(0)) {
							program.startEncode(path, EncondingType.getByCode(tipo), false, divisor);
						} else {
							throw new NumberFormatException();
						}
					} catch (NumberFormatException e) {
						Util.exit("O quinto parametro deve ser 0 ou 1 para indicar se a codificação usará hamming.");
					}

				} else {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException e) {
				System.out.println("O segundo parametro deve ser um inteiro correspondente ao tipo da codificação... e o terceiro o divisor (informe 0 caso não use.");
				Util.exit("(0: Golomb, 1:Elias-Gamma, 2:Fibonacci e 3:Unária");
			}

		} else if (args[0].equalsIgnoreCase("D")) {
			if (args.length != 3) {
				Util.exit("Procedimento de Decode exige 5 parametros...");
			}

			Path path = Path.of(args[1]);

			try {
				Integer hamming = Integer.parseInt(args[2]);
				if (hamming.equals(1)) {
					program.startDecode(path, true);
				} else if (hamming.equals(0)) {
					program.startDecode(path, false);
				} else {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException e) {
				Util.exit("O segundo parametro deve ser 0 ou 1 para indicar se a codificação usará hamming.");
			}

		} else {
			Util.exit("informe no primeiro argumento: E: encode ou D: decode...");
		}

		// Nome do arquivo na pasta resources
//		Path arquivoEntrada = Util.pathResources("arquivo");
//
//		Program program = new Program();
//		// Encode
//		program.startEncode(arquivoEntrada, EncondingType.Fibonacci, true);
//		program.startEncode(arquivoEntrada, EncondingType.Unary, true);
//		program.startEncode(arquivoEntrada, EncondingType.EliasGama, true);
//		program.startEncode(arquivoEntrada, EncondingType.Golomb, true, 4);
//
//		// Decodes
//		arquivoEntrada = Util.pathResources("Golomb_arquivo.ecc");
//		program.startDecode(arquivoEntrada, true);
//		arquivoEntrada = Util.pathResources("Unary_arquivo.ecc");
//		program.startDecode(arquivoEntrada, true);
//		arquivoEntrada = Util.pathResources("Fibonacci_arquivo.ecc");
//		program.startDecode(arquivoEntrada, true);
//		arquivoEntrada = Util.pathResources("EliasGama_arquivo.ecc");
//		program.startDecode(arquivoEntrada, true);
	}
}
