package br.unisinos.edu.teoria.controller;

import java.io.IOException;
import java.nio.file.Path;

import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.model.roots.IDecoder;
import br.unisinos.edu.teoria.model.roots.IEncoder;

public abstract class EncoderDecoderProgram implements IEncoder, IDecoder {

	private Path inFile;
	private Path outFile;
	private EncondingType type;
	private boolean printReadBytes = false;
	protected final char BIT_1 = '1';
	protected final char BIT_0 = '0';

	public EncoderDecoderProgram(EncondingType type) {
		this.type = type;
	}

	public EncoderDecoderProgram(EncondingType type, boolean printReadBytes) {
		this.type = type;
		this.printReadBytes = printReadBytes;
	}

	public EncondingType getType() {
		return type;
	}

	public abstract byte[] encodeLogic(byte[] bytes);

	public abstract byte[] decodeLogic(byte[] bytes);

	protected void printBytesArray(byte[] bytes) {
		System.out.println("Em ASCII:....\t");
		for (final byte b : bytes) {
			System.out.print(b);
		}
		System.out.println("\n");
		System.out.println("Em Texto:....\t");
		System.out.print(new String(bytes));
		System.out.println("\n");
	}

	@Override
	public byte[] headerWrite() throws IOException {
		return new byte[] { type.getCode(), (byte) '0' };
	}

	@Override
	public byte[] encode(byte[] bytes) throws IOException {

		System.out.println("Verificando array de bytes...");
		System.out.println("Lido " + bytes.length + " bytes...");

		// PrintBytes
		if (printReadBytes) {
			printBytesArray(bytes);
		}

		// Codificacao
		System.out.println("Codificando...");
		byte[] encodedBytes = encodeLogic(bytes);
		System.out.println("Codificacao finalizada...");

		return encodedBytes;
	}

	@Override
	public byte[] decode(byte[] bytes) throws IOException {

		System.out.println("Verificando array de bytes...");
		System.out.println("Lido " + bytes.length + " bytes...");

		// PrintBytes
		if (printReadBytes) {
			printBytesArray(bytes);
		}

		// Codificacao
		System.out.println("Decodificando...");
		byte[] encodedBytes = decodeLogic(bytes);
		System.out.println("Decodificacao finalizada...");

		return encodedBytes;
	}

	public Path getInFile() {
		return inFile;
	}

	public void setInFile(Path inFile) {
		this.inFile = inFile;
	}

	public Path getOutFile() {
		return outFile;
	}

	public void setOutFile(Path outFile) {
		this.outFile = outFile;
	}

}
