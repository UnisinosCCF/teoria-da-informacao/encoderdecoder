package br.unisinos.edu.teoria.util;

import java.io.File;
import java.nio.file.Path;

/**
 * Classe utilitaria.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 12/09/2020
 */
public class Util {

	/**
	 * Retorna o caminho da pasta de recursos do sistema.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 12/09/2020
	 * @return
	 */
	public static Path pathResources() {
		try {
			return Path.of(Util.class.getResource("../resources").toURI());
		} catch (Exception e) {
			System.out.println("Impossivel resolver o Path");
			e.printStackTrace();
			return null;
		}
	}

	public static String addToLeft(String original, String add, int occurrences) {
		String out = "";
		for (int i = 0; i < occurrences; i++) {
			out += add;
		}
		return out + original;
	}

	public static String addToRight(String original, String add, int occurrences) {
		String out = "";
		for (int i = 0; i < occurrences; i++) {
			out += add;
		}
		return original + out;
	}

	public static Path changeSufixFile(Path current, String newSufix) {
		File newFile = new File(current.getParent().toFile().getAbsolutePath() + File.separator + current.toFile().getName() + newSufix);
		return newFile.toPath();
	}

	public static void main(String[] args) {
		System.out.println(Util.addToLeft("1001", "0", 2));

	}

	/**
	 * Retorna o caminhoda pasta de recursos somado com os paths informados por
	 * parametro.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 12/09/2020
	 * @param paths
	 * @return
	 */
	public static Path pathResources(String... paths) {
		Path thisPath = pathResources();
		return Path.of(thisPath.toString(), paths);
	}

	/**
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de set. de 2020
	 * @param message
	 */
	public static void exit(String message) {
		System.out.println(message);
		System.out.println("\n\tFinalizando sistema...........�\\_(*-*)_/�");
		System.exit(0);
	}

	/**
	 * Concatena na saída cada símbolo codificado
	 * 
	 * @author Fabiane Kuhn <fabianeekuhn@gmail.com>
	 * @since 25 de set. de 2020
	 * @param saida, temp
	 * @return
	 */
	public static byte[] updateOutput(byte[] saida, byte... temp) {
		int aLen = saida.length;
		int bLen = temp.length;
		byte[] result = new byte[aLen + bLen];

		System.arraycopy(saida, 0, result, 0, aLen);
		System.arraycopy(temp, 0, result, aLen, bLen);

		return result;
	}

	/**
	 * Calcula qualquer base de log.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 3 de out. de 2020
	 * @param base
	 * @param valor
	 * @return
	 */
	public static double log(int base, int valor) {
		return (Math.log(valor) / Math.log(base));
	}

	// public static void main(String[] args) {
	// System.out.println(Util.pathResources());
	// System.out.println(Util.pathResources("novo", "velho", "arquivo.txt"));
	// }
}
