package br.unisinos.edu.teoria.util;

import java.util.zip.Checksum;

/**
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 4 de out. de 2020
 */
public class CRC8 implements Checksum {
	private static final int poly = 0x0D5;
	private int crc = 0;

	@Override
	public void update(final byte[] input, final int offset, final int len) {
		for (int i = 0; i < len; i++) {
			update(input[offset + i]);
		}
	}

	public void update(final byte[] input) {
		update(input, 0, input.length);
	}

	private final void update(final byte b) {
		crc ^= b;
		for (int j = 0; j < 8; j++) {
			if ((crc & 0x80) != 0) {
				crc = ((crc << 1) ^ poly);
			} else {
				crc <<= 1;
			}
		}
		crc &= 0xFF;
	}

	@Override
	public void update(final int b) {
		update((byte) b);
	}

	@Override
	public long getValue() {
		return (crc & 0xFF);
	}

	@Override
	public void reset() {
		crc = 0;
	}

	public static Integer getCRC8(byte... bytes) {
		CRC8 crc = new CRC8();
		crc.reset();
		crc.update(bytes);
		return (int) crc.getValue();
	}

	public static String getCRC8InBinary(byte... bytes) {
		String bin = Integer.toBinaryString(getCRC8(bytes));
		if (bin.length() < 8) {
			bin = Util.addToLeft(bin, "0", (8 - bin.length()));
		}
		return bin;
	}

//	/**
//	 * Test
//	 */
//	public static void main(String[] args) {
//		CRC8 crc = new CRC8();
//		crc.reset();
////		crc.update("4".getBytes());
//		crc.update("SSSSSSSS".getBytes());
//		System.out.println(crc.getValue());
//		System.out.println(Integer.toBinaryString((int) crc.getValue()));
//		System.out.println(CRC8.getCRC8(new byte[] { '0', '4' }));
//		System.out.println(CRC8.getCRC8InBinary(new byte[] { '0', '4' }));
//	}

}