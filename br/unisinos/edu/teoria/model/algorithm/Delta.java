package br.unisinos.edu.teoria.model.algorithm;

import br.unisinos.edu.teoria.controller.EncoderDecoderProgram;
import br.unisinos.edu.teoria.model.roots.EncondingType;

public class Delta extends EncoderDecoderProgram {

	public Delta() {
		super(EncondingType.Delta);
	}

	public Delta(boolean print) {
		super(EncondingType.Delta, print);
	}

	/**
	 * Usado para obter o maior degrau entre os simbolos, o menor e o simbolo de
	 * maior valor
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 4 de out. de 2020
	 * @param bytes
	 * @return
	 */
	private Integer[] symbolAnalysis(byte[] bytes) {
		Integer rv[] = new Integer[] { 0, 0, 0 };
		// rv[0] = maior simbolo;
		// rv[1] = maior degrau;
		// rv[2] = menor degrau;
		for (int i = 0; i < bytes.length; i++) {
			int current = bytes[i];
			// Analise de maior simbolo
			if (current > rv[0]) {
				rv[0] = current;
			}
			// Analise de degrau
			if ((i + 1) < bytes.length) {
				int next = bytes[i + 1];
				int step = -(current - next);
				if (step > rv[1]) {
					rv[1] = step;
				} else if (step < rv[2]) {
					rv[2] = step;
				}
				// Se for igual é zero e significa sem degrau.
			}
		}
		return rv;
	}

	/**
	 * Deve servir para retorna qual é a codificacao que representa esse degrau.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 4 de out. de 2020
	 * @param numeroBits
	 * @param maiorDegrau
	 * @param degrau
	 * @return
	 */
	private String getBitsStep(int numeroBits, int maiorDegrau, int degrau) {
		int numero = maiorDegrau - (degrau);

		return "";
	}

	@Override
	/**
	 * A l�gica da compacta��o deve ser feita aqui.
	 */
	public byte[] encodeLogic(byte[] bytes) {

		// Pega o maior degrau.
		Integer[] analysis = symbolAnalysis(bytes);
		Integer degrauSize = (Math.abs(analysis[1]) + Math.abs(analysis[2]));
		System.out.println("Maior simbolo codigo: " + analysis[0]);
		System.out.println("Maior degrau: " + analysis[1]);
		System.out.println("Menor degrau: " + analysis[2]);
		System.out.println("Tamanhos de bits dos degraus: " + degrauSize + " (" + Integer.toBinaryString(degrauSize) + ")");
		Integer numeroBits = Integer.toBinaryString(degrauSize).length();

		byte[] temp = null;
		byte[] saida = {};

		return bytes;
	}

	@Override
	public byte[] decodeLogic(byte[] bytes) {
		return bytes;
	}

}
