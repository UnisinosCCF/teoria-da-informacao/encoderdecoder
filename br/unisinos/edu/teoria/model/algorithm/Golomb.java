package br.unisinos.edu.teoria.model.algorithm;

import java.io.IOException;

import br.unisinos.edu.teoria.controller.EncoderDecoderProgram;
import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.util.Util;

/**
 * Classe responsavel por tratar encode e decode golomb
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 3 de out. de 2020
 */
public class Golomb extends EncoderDecoderProgram {

	private Integer divider = 4;
	private final byte STOP_BIT = BIT_1;
	private final byte PREFIX_BIT = BIT_0;

	public Golomb(Integer divider) {
		super(EncondingType.Golomb);
		this.divider = divider;
	}

	public Golomb(boolean print, Integer divider) {
		super(EncondingType.Golomb, print);
		this.divider = divider;
	}

	/**
	 * Monta o prefixo do codeword
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 3 de out. de 2020
	 * @param value
	 * @return
	 */
	private byte[] buildPrefix(Integer value) {
		int nPrefix = (value - (value % divider)) / divider;
		byte[] bytes = new byte[nPrefix];
		for (int i = 0; i < nPrefix; i++) {
			bytes[i] = (PREFIX_BIT);
		}
		return bytes;
	}

	private Integer sizeSufix() {
		return ((int) Util.log(2, divider));
	}

	/**
	 * Monta o prefixo do codeword
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 3 de out. de 2020
	 * @param value
	 * @return
	 */
	private byte[] buildSufix(Integer value, Integer size) {
		String binary = Integer.toBinaryString(value % divider);
		// Verificar se o tamanho do binario cotempla o tamanho do sufixo;
		if (binary.length() > size) {
			Util.exit("Ops! houve falha ao gerar um sufixo de golomb!!!");
		} else if (binary.length() < size) {
			int diff = size - binary.length();
			binary = Util.addToLeft(binary, "0", diff);
			// System.err.println(binary);
		}
		return binary.getBytes();
	}

	/**
	 * O Metodo header precisa ser reescrito no Golomb pois ele precisa informar o
	 * divisor no segundo byte.
	 */
	@Override
	public byte[] headerWrite() throws IOException {
		if (divider == null || divider < 1) {
			Util.exit("Em codificacao Golomb e necessario ser informado um divisor >= 1");
		}
		return new byte[] { getType().getCode(), divider.toString().getBytes()[0] };
	}

	@Override
	/**
	 * A l�gica da compacta��o deve ser feita aqui.
	 */
	public byte[] encodeLogic(byte[] bytes) {

		int n;
		byte[] temp = null;
		byte[] saida = {};
		int tamanhoSufixo = sizeSufix();
		for (int i = 0; i < bytes.length; i++) {
			n = bytes[i];

			// Cria o prefixo;
			temp = buildPrefix(n);
			saida = Util.updateOutput(saida, temp);
			// Adiciona o stopbit
			saida = Util.updateOutput(saida, STOP_BIT);
			// Cria o sufixo
			temp = buildSufix(n, tamanhoSufixo);
			saida = Util.updateOutput(saida, temp);

		}

		return saida;
	}

	@Override
	public byte[] decodeLogic(byte[] bytes) {

		int somaPrefix = 0;
		int tamanhoSufixo = sizeSufix();
		byte[] saida = {};
		for (int i = 0; i < bytes.length; i++) {
			int n = bytes[i];
			if (n == (byte) '0') {
				somaPrefix++;
			} else if (n == (byte) '1') {
				i++;
				String somaSufix = "";
				int tempSufix = 0;
				for (int j = i; tempSufix < tamanhoSufixo; j++) {
					somaSufix += new String(bytes, j, 1);
					tempSufix++;
					i = j;
				}

				saida = Util.updateOutput(saida, ((byte) (char) ((somaPrefix * divider) + Integer.parseInt(somaSufix, 2))));
				somaPrefix = 0;
			}

		}

		return saida;
	}

}
