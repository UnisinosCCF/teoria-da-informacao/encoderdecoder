package br.unisinos.edu.teoria.model.algorithm;

import br.unisinos.edu.teoria.controller.EncoderDecoderProgram;
import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.util.Util;

public class EliasGama extends EncoderDecoderProgram {

	public EliasGama() {
		super(EncondingType.EliasGama);
	}

	public EliasGama(boolean print) {
		super(EncondingType.EliasGama, print);
	}

	public byte[] unary(int log) {
		byte logN[] = new byte[log + 1];
		int i = 0;
		while (i < log) {
			logN[i] = '0';
			i++;
		}
		logN[log] = '1';
		return logN;
	}

	private byte[] binary(int log, final int n, byte[] temp) {
		String s = Integer.toBinaryString(n);
		temp = s.getBytes();
		int dif = 0;
		byte saida[] = new byte[log];
		if (temp.length < log) {
			dif = log - temp.length;
			for (int j = 0; j < dif; j++) {
				saida[j] = '0';
			}
		}
		for (int i = 0; i < temp.length; i++) {
			saida[dif + i] = temp[i];
		}
		return saida;
	}

	private int decodeBinario(byte[] binario, int i, int ind) {
		int result = 0;
		for (int j = i + 1; j < i + 1 + ind; j++) {
			if (binario[j] == '1') {
				result += Math.pow(2, (i + ind - j));
			}
		}
		return result;
	}

	@Override
	public byte[] encodeLogic(byte[] bytes) {
		byte[] saida = {};
		byte[] temp = null;
		int n = bytes[0];
		int i;
		int log;
		int resto;
		for (int j = 0; j < bytes.length; j++) {
			n = bytes[j];
			log = (int) (Math.log(n) / Math.log(2));
			temp = unary(log);
			saida = Util.updateOutput(saida, temp);
			resto = n - (int) (Math.pow(2, log));
			temp = binary(log, resto, temp);
			saida = Util.updateOutput(saida, temp);
		}
		return saida;
	}

	@Override
	public byte[] decodeLogic(byte[] bytes) {
		int result = 0;
		byte[] saida = {};
		int ind = 0;
		for (int i = 0; i < bytes.length; i++) {
			// Conta o número de zeros
			if ((bytes[i] == '1') && (bytes.length > i + 1)) {
				result += Math.pow(2, ind);
				result += decodeBinario(bytes, i, ind);
				i = i + ind;
				ind = 0;
				saida = Util.updateOutput(saida, (byte) result);
				result = 0;
			} else {
				ind++;
			}
		}
		return saida;
	}
}