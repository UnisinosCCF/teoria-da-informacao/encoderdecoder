package br.unisinos.edu.teoria.model.algorithm;

import br.unisinos.edu.teoria.controller.EncoderDecoderProgram;
import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.util.Util;

public class Unary extends EncoderDecoderProgram {

	public Unary() {
		super(EncondingType.Unary);
	}

	public Unary(boolean print) {
		super(EncondingType.Unary, print);
	}

	@Override

	public byte[] encodeLogic(byte[] bytes) {
		byte[] saida = {};
		byte[] temp = null;
		int n = bytes[0];
		int i;
		for (int j = 0; j < bytes.length; j++) {
			n = bytes[j];
			temp = new byte[n + 1];
			i = 0;
			while (i < n) {
				temp[i] = '0';
				i++;
			}
			temp[n] = '1';
			saida = Util.updateOutput(saida, temp);
		}
		return saida;
	}

	@Override
	public byte[] decodeLogic(byte[] bytes) {
		int n = 0;
		byte[] saida = {};
		for (int i = 0; i<bytes.length; i++){
			if (bytes[i] == '0') {
				n++;
			} else{
				saida = Util.updateOutput(saida, (byte)n);
				n = 0;
			}
		}
		return saida;
	}
}
