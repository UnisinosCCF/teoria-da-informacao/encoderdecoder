package br.unisinos.edu.teoria.model.algorithm;

import br.unisinos.edu.teoria.controller.EncoderDecoderProgram;
import br.unisinos.edu.teoria.model.roots.EncondingType;
import br.unisinos.edu.teoria.util.Util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class Fibonacci extends EncoderDecoderProgram {

	private final int sizeFibonacci = 30;

	public Fibonacci() {
		super(EncondingType.Fibonacci);
	}

	public Fibonacci(boolean print) {
		super(EncondingType.Fibonacci, print);
	}

	/**
	 * Metodo para encontrar o índice do maior fibonacci menor que o valor a ser
	 * codificado
	 *
	 * @author Fabiane Kuhn <fabianeekuhn@gmail.com>
	 * @since 22 de set. de 2020
	 * @param n
	 * @return
	 */

	public int largestFiboLessOrEqual(int n, int fib[]) {
		// Inicializa a sequência Fibonacci
		fib[0] = 1;
		fib[1] = 2;
		// Gera os próximos números Fibonacci enquanto forem menores que o valor a ser
		// codificado
		int i;
		for (i = 2; fib[i - 1] <= n; i++)
			fib[i] = fib[i - 1] + fib[i - 2];
		return (i - 2);
	}

	public int calcFibonacciDecoder(int i){
		int fib[] = new int [sizeFibonacci];
		fib[0]= 1;
		fib[1] = 2;
		for (int j=2; j<i+1; j++){
			fib[j] = fib[j-1] + fib[j-2];
		}
		return fib[i];
	}
	@Override

	public byte[] encodeLogic(byte[] bytes) {
		// Estipula limite para sequencia de Fibonacci
		int[] fib = new int[sizeFibonacci];
		// Variável responsável pela interação
		int n = bytes[0];
		byte[] temp = null;
		byte[] saida = {};
		for (int j = 0; j < bytes.length; j++) {
			n = bytes[j];
			// Encontra o índice do maior fibonacci menor que o valor a ser codificado
			int index = largestFiboLessOrEqual(n, fib);
			int i = index;
			temp = new byte[index + 2];
			while (n != 0) {
				temp[i] = '1';
				n = n - fib[i];
				i = i - 1;
				// Marca todos os Fibonacci's maiores que o valor a ser codificado como não
				// utilizados (bit 0)
				while (i >= 0 && fib[i] > n) {
					temp[i] = '0';
					i = i - 1;
				}
			}
			// Adiciona o stopbit '1'
			temp[index + 1] = '1';
			saida = Util.updateOutput(saida, temp);

		}
		// retorna saída codificada
		return saida;
	}

	@Override
	public byte[] decodeLogic(byte[] bytes) {
		int result = 0;
		byte[] saida ={};
		int prev = 0;
		for (int i=0; i<bytes.length -1; i++){
			// Procura stop bit
			if ((bytes[i] == '1') && (bytes[i+1] == '1')){
				int j = i;
				for (int x = prev; x<j+1; x++) {
					if (bytes[x] == '1') {
						result += calcFibonacciDecoder(x-prev);
					}
				}
				saida = Util.updateOutput(saida, (byte)result);
				result = 0;
				prev = i+2;
				i = prev;
			}
		}
		return saida;
	}
}
