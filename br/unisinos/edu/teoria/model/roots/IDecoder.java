package br.unisinos.edu.teoria.model.roots;

import java.io.IOException;

/**
 * Interface que define as regras do decode.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 4 de out. de 2020
 */
public interface IDecoder {

	public byte[] decode(byte[] bytes) throws IOException;

}
