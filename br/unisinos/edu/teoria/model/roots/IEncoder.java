package br.unisinos.edu.teoria.model.roots;

import java.io.IOException;

/**
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 21 de set. de 2020
 */
public interface IEncoder {

	public byte[] encode(byte[] bytes) throws IOException;

	public byte[] headerWrite() throws IOException;

}
