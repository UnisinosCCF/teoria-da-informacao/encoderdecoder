package br.unisinos.edu.teoria.model.roots;

import br.unisinos.edu.teoria.util.Util;

public enum EncondingType {
	Golomb('0'), EliasGama('1'), Fibonacci('2'), Unary('3'), Delta('4');

	private final byte code;

	private EncondingType(final char code) {
		this.code = (byte) code;
	}

	public byte getCode() {
		return code;
	}

	public static EncondingType getByCode(Integer codee) {
		char value = Character.forDigit(codee, 10);
		return getByCode((byte) value);
	}

	/**
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 4 de out. de 2020
	 * @param type
	 * @return
	 */
	public static EncondingType getByCode(byte bytee) {
		for (EncondingType et : values()) {
			if (et.code == bytee) {
				return et;
			}
		}
		Util.exit("Tipo de Algoritimo inv�lido: " + bytee);
		return null;
	}
}