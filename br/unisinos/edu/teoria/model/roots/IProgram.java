package br.unisinos.edu.teoria.model.roots;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Interface que define as regras do programa.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 4 de out. de 2020
 */
public interface IProgram {

	public void startEncode(Path pathFileIN, Path pathFileOUT, EncondingType type, boolean eccHamming, Integer... attrs) throws IOException;

	public default void startEncode(Path pathFileIN, EncondingType type, boolean eccHamming, Integer... attrs) throws IOException {
		startEncode(pathFileIN, Path.of(pathFileIN.getParent().toString(), type.name() + "_" + pathFileIN.getFileName()), type, eccHamming, attrs);
	}

	public void startDecode(Path pathFileIN, Path pathFileOUT, boolean eccHamming) throws IOException;

	public default void startDecode(Path pathFileIN, boolean eccHamming) throws IOException {
		startDecode(pathFileIN, Path.of(pathFileIN.getParent().toString(), "Decodificado_" + pathFileIN.getFileName()), eccHamming);
	}

	public byte[] headerRead(byte[] bytes) throws IOException;

	public byte[] read(Path pathFile) throws IOException;

	public void write(boolean append, Path pathFile, byte... bytes) throws IOException;

	public default void write(Path pathFile, byte... bytes) throws IOException {
		write(false, pathFile, bytes);
	}

}
